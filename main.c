#include <stdio.h>

#include <point.h>

int main() 
{
 	//declaro los puntos a, b
	
	Point a,b,c;
	//pido al usuario que ingrese un punto usando la funcion newPoint
 	printf("Ingrese los datos del primer punto a continuacion \n");
	a=newPoint();
	printf("Ingrese los datos del segundo punto a continuacion \n");
	b=newPoint();
	printf("La distancia euclidiana es: %f metros \n", euclideanDistance(a,b));
	c=midPoint(a,b);
	printf("El punto medio es\n");
	printf("x: %f \ny: %f \nz: %f\n",c.x,c.y,c.z);

return 0;

}

