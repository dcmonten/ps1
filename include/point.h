
//estructura Point
typedef struct Point 
{
	float x;
	float y;
	float z;
} Point;


float euclideanDistance(Point a, Point b);
Point newPoint(void);
Point midPoint(Point a, Point b);

