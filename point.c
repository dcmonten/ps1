#include <stdio.h>
#include <math.h>
#include <point.h>



//distancia euclidiana
float euclideanDistance(Point a, Point b)
{

	if (a.x==b.x && a.y==b.y && a.z==b.z)return 0;
	else return sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2)+pow(a.z-b.z,2));

}

//crear un punto preguntando cordenadas al usuario
Point newPoint(void)
{
	Point a;//punto vacio
	printf("Ingrese la coordenada en x del punto: \n");
	scanf("%f",&a.x);
 	printf("Ingrese la coordenada en y del punto: \n");
	scanf("%f",&a.y);
 	printf("Ingrese la coordenada en z del punto: \n");
	scanf("%f",&a.z);
	return a;
}

//toma dos puntos y retorna el punto medio
Point midPoint(Point a, Point b)
{
	float x,y,z;
	Point mid;

	x=(a.x+b.x)/2.0;
	y=(a.y+b.y)/2.0;
	z=(a.z+b.z)/2.0;
	
	mid.x=x;
	mid.y=y;
	mid.z=z;

	return mid;
}
