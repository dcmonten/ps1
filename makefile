IDIR =./include
CC=gcc
CFLAGS=-I$(IDIR) -lm 

OBJ=main.c point.c
EXECUTABLE=tarea

all: $(EXECUTABLE)

_DEPS = point.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

$(EXECUTABLE):$(OBJ)
	$(CC) -o $(EXECUTABLE) $(EXECUTABLE) $(CFLAGS)

$(EXECUTABLE): main.o point.o
	$(CC) -o $(EXECUTABLE) main.o point.o $(CFLAGS)

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm *o $(EXECUTABLE)
